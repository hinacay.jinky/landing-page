const express = require("express");
const cors = require("cors");
const fs = require("fs");

const app = express();
app.use(express.json());
app.use(cors());

// GET method route
//@ts-ignore
app.post("/save", (req, res) => {
  const body = req.body;

  setTimeout(() => {
    //@ts-ignore
    fs.writeFile("user.txt", JSON.stringify(body), (err) => {
      if (err) {
        console.error(err);
      } else {
        // file written successfully
        res.json({ requestBody: req.body }); // <==== req.body will be a parsed JSON object
      }
    });
  }, 2000);
});

app.listen(3002, () => {
  console.log("RUNNING ON PORT 3002");
});
