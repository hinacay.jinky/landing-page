import "react-phone-number-input/style.css";

import React, { useEffect, useState } from "react";
import axios from "axios";
import PhoneInput from "react-phone-number-input";

//@ts-ignore
import { TextField, Alert } from "@mui/material";
import { LoadingButton } from "@mui/lab";

import "./App.css";

const App = () => {
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    postCode: "",
    phoneNumber: "",
  });
  const [isSaving, setIsSaving] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const handleSaveUser = async () => {
    try {
      setIsSaving(true);
      await axios.post("http://localhost:3002/save", {
        user,
      });

      setUser({
        firstName: "",
        lastName: "",
        email: "",
        postCode: "",
        phoneNumber: "",
      });
      setIsSaving(false);
      setShowAlert(true);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (showAlert) {
      setTimeout(() => {
        setShowAlert(false);
      }, 1500);
    }
  }, [showAlert]);

  return (
    <div className="App">
      <div className="App-form">
        <TextField
          id="outlined-basic"
          label="First Name"
          variant="outlined"
          value={user.firstName}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setUser({ ...user, firstName: event.target.value });
          }}
        />
        <TextField
          id="outlined-basic"
          label="Last Name"
          variant="outlined"
          value={user.lastName}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setUser({ ...user, lastName: event.target.value });
          }}
        />
        <TextField
          id="outlined-basic"
          label="Email Address"
          variant="outlined"
          value={user.email}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setUser({ ...user, email: event.target.value });
          }}
        />
        <TextField
          id="outlined-basic"
          label="Post Code"
          variant="outlined"
          value={user.postCode}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
            setUser({ ...user, postCode: event.target.value });
          }}
        />
        <PhoneInput
          placeholder="Enter phone number"
          value={user.phoneNumber}
          onChange={(value: any) =>
            setUser({
              ...user,
              phoneNumber: value,
            })
          }
        />
        <LoadingButton
          loading={isSaving}
          variant="contained"
          color="primary"
          onClick={handleSaveUser}
        >
          Save User
        </LoadingButton>
        {showAlert && (
          <Alert variant="filled" severity="success">
            User successfully saved!
          </Alert>
        )}
      </div>
    </div>
  );
};

export default App;
