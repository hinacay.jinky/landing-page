## Assumptions

My assumptions in creating this challenge are to test my skills and knowledge in creating an application and my experience in using the following technologies, such as React and Node, which are required in the role to which I am applying.

Another is to test my CRUD skills, which are considered necessary to implement if making a storage application such as this one.

## Commands

To run the app

1. Open two terminals, one for the client and one for the server.
2. Install dependencies. run this command: yarn
3. For the server termninal, navigate to the sever folder, and then exeute this command: node server.ts
4. For the client terminal, run: yarn start
5. Open the browser: http:localhost:3000 to test the app.

In the app

1. Input all the details needed in the app.
2. After saving, check the user.txt file under the server folder to see the newly saved user information.
